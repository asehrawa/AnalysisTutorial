#!/usr/bin/env python

import os
import argparse
import ROOT

# Initialize the ROOT system
ROOT.xAOD.Init().ignore()

# Setup argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config-path', dest='configPath', action='store', required=True, type=str, help='Path to the YAML configuration file.')
parser.add_argument('-s', '--submission-dir', dest='submitDir', action='store', type=str, default='submitDir', help='Directory for job submission outputs')
options = parser.parse_args()

# Set data type: 'data' or 'mc'
dataType = 'mc'

# Setup SampleHandler to manage the input files
sh = ROOT.SH.SampleHandler()
sh.setMetaString('nc_tree', 'CollectionTree')

# Configure SampleHandler to scan datasets using Rucio
if dataType == 'mc':
    ROOT.SH.scanRucio(sh, 'mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_a907_r14861_p6117')
else:
    # Similar setup for data
    pass

# Print configured samples
sh.printContent()

# Create an EventLoop job
job = ROOT.EL.Job()
job.sampleHandler(sh)
job.options().setDouble(ROOT.EL.Job.optMaxEvents, 500)  # Adjust as necessary
job.options().setString(ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Configure the analysis algorithm
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm('MyxAODAnalysis', 'AnalysisAlg')

# Configure an algSequence using the YAML config
from MyAnalysis.MyAnalysisAlgorithms import makeSequence
algSeq = makeSequence(options.configPath, dataType)
algSeq.addSelfToJob(job)

# Add algorithm to the job
job.algsAdd(alg)
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Setup the PrunDriver for grid job submission
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", f"user.asehrawa.{dataType}_output")  # Manage output dataset name dynamically
driver.options().setString("nc_mergeOutput", "true")  # Merge outputs into a single file for easier management

# Submit the job using the PrunDriver without waiting for completion
driver.submitOnly(job, options.submitDir)

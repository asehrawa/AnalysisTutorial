#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <TTree.h>

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. This is also where you
  // declare all properties for your algorithm. Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees. This method gets called before any input files are
  // connected.

  ANA_CHECK(book( TTree("analysis", "Result Tree") ));
  m_tree = tree("analysis");

  m_tree->Branch("electron_pt_", &electron_pt_);
  m_tree->Branch("electron_eta_", &electron_eta_);
  m_tree->Branch("electron_phi_", &electron_phi_);
  m_tree->Branch("muon_pt_", &muon_pt_);
  m_tree->Branch("muon_eta_", &muon_eta_);
  m_tree->Branch("muon_phi_", &muon_phi_);
  m_tree->Branch("jet_pt_", &jet_pt_);
  m_tree->Branch("jet_eta_", &jet_eta_);
  m_tree->Branch("jet_phi_", &jet_phi_);
  m_tree->Branch("met_et_", &met_et_);
  m_tree->Branch("met_phi_", &met_phi_);
  m_tree->Branch("runNumber_", &m_runNumber_);
  m_tree->Branch("eventNumber_", &m_eventNumber_);

  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees. This is where most of your actual analysis
  // code will go.
  const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    m_runNumber_ = eventInfo->runNumber();
    m_eventNumber_ = eventInfo->eventNumber();

    const xAOD::ElectronContainer* electrons = nullptr;
    ANA_CHECK(evtStore()->retrieve(electrons, "Electrons"));
    const xAOD::MuonContainer* muons = nullptr;
    ANA_CHECK(evtStore()->retrieve(muons, "Muons"));
    const xAOD::JetContainer* jets = nullptr;
    ANA_CHECK(evtStore()->retrieve(jets, "AntiKt4EMTopoJets"));
    const xAOD::MissingETContainer* met = nullptr;
    ANA_CHECK(evtStore()->retrieve(met, "MET_Core_AntiKt4EMPFlow"));

    electron_pt_.clear();
    electron_eta_.clear();
    electron_phi_.clear();
    for (const xAOD::Electron* electron : *electrons) {
      electron_pt_.push_back(electron->pt());
      electron_eta_.push_back(electron->eta());
      electron_phi_.push_back(electron->phi());
    }

    muon_pt_.clear();
    muon_eta_.clear();
    muon_phi_.clear();
    for (const xAOD::Muon* muon : *muons) {
      muon_pt_.push_back(muon->pt());
      muon_eta_.push_back(muon->eta());
      muon_phi_.push_back(muon->phi());
    }

    jet_pt_.clear();
    jet_eta_.clear();
    jet_phi_.clear();
    for (const xAOD::Jet* jet : *jets) {
      jet_pt_.push_back(jet->pt());
      jet_eta_.push_back(jet->eta());
      jet_phi_.push_back(jet-> phi());
    }

    met_et_ = met->at(0)->met(); 
    met_phi_ = met->at(0)->phi();

    m_tree->Fill();

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk. This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

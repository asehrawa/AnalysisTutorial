# MyAnalysis

Clone only the source/ repository directly in your afs/ home area inside a directory named AnalysisTutorial,

git clone ssh://git@gitlab.cern.ch:7999/asehrawa/AnalysisTutorial.git

Inside the same directory, mkdir run/ and build/

# Set up ATLAS environment

setupATLAS

diagnostics

setMeUp cern-jun2024

# Compilation

cd build/

asetup --stable 25.2,AnalysisBase,25.2.12

cmake ../source

make -j

source x86_64-el9-gcc13-opt/setup.sh


# Run job script with CP algorithms (config.yaml) file

cd ../run

export ALRB_TutorialData=/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024

ATestRun_eljob.py --config-path=../source/MyAnalysis/data/config.yaml --submission-dir=submitDir

# Ntuples root file

root submitDir/data-ANALYSIS/dataset.root

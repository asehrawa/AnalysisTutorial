#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // This is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // These are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  // Configuration, and any other types of variables go here.
  TTree* m_tree = nullptr; // The TTree to output data
  std::vector<float> electron_pt_, electron_eta_, electron_phi_;
  std::vector<float> muon_pt_, muon_eta_, muon_phi_;
  std::vector<float> jet_pt_, jet_eta_, jet_phi_;
  float met_et_ = 0.0, met_phi_ = 0.0;
  unsigned int m_runNumber_ = 0;
  unsigned long long m_eventNumber_ = 0;

};

#endif

# Cloning repository

git clone ssh://git@gitlab.cern.ch:7999/asehrawa/AnalysisTutorial.git

See instructions: https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/

# Set up ATLAS environment

setupATLAS

diagnostics

setMeUp cern-jun2024

cd build

asetup --stable 25.2,AnalysisBase,25.2.12

# Compiling 

cmake ../source

make -j

source x86_64-el9-gcc13-opt/setup.sh

# Running job script with CP algorithms to create ntuples
cd ../run

export ALRB_TutorialData=/cvmfs/atlas.cern.ch/repo/tutorials/asg/cern-mar2024

ATestRun_eljob.py --config-path=../source/MyAnalysis/data/config.yaml --submission-dir=submitDir

# Output root file containing ntuples

root submitDir/data-ANALYSIS/dataset.root


# Run Job on the Grid


<!--

- **Modify the Job Submission Script**

Adjust `ATestRun_eljob.py` to handle grid submission by creating a new version for the grid, such as `ATestSubmit_eljob.py`:

cp ATestRun_eljob.py ATestSubmit_eljob.py

Edit `ATestSubmit_eljob.py` to use grid data and PrunDriver:

- **Change Data Input Handling** to use Rucio datasets:

  import ROOT

  sh = ROOT.SH.SampleHandler()

  ROOT.SH.scanRucio(sh, 'mc20_13TeV.312276.aMcAtNloPy8EG_A14N30NLO_LQd_mu_ld_0p3_beta_0p5_2ndG_M1000.deriv.DAOD_PHYS.e7587_a907_r14861_p6117')

- **Change the Driver** to `PrunDriver`:

  driver = ROOT.EL.PrunDriver()

  driver.options().setString("nc_outputSampleName", "user.asehrawa.grid_test_run")

  driver.options().setString("nc_mergeOutput", "true")

- **Modify the job submission method** to submitOnly(), which submits the job without waiting for it to complete

  driver.submitOnly(sh, "submitDir")

-->
- **Setup PanDA**:

  lsetup panda

- **Submit the Job to the Grid**:

  ATestSubmit_eljob.py --config-path=../source/MyAnalysis/data/config.yaml --submission-dir=submitDir

- **Monitor the Job** using BigPanDA:

  https://bigpanda.cern.ch

  Enter your `jediTaskID` to track the job status.


<!-- 
- **Access and Analyze the Output**

Download the output using Rucio once the job completes:

lsetup rucio

rucio download user.asehrawa.AnalysisOutput


- **Debugging and Logs**

If necessary, check the job logs via BigPanDA for any issues to debug.

-->

# Sidenote

Skimming: removal of whole events

Thinning: removal of whole objects from a container within an event

Slimming: removal of branches (variables) from all objects in a container, applied uniformly for each event